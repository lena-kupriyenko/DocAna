import java.io.*;
import java.util.*;

/**
 * For implementing part-of-speech tagger we have used baseline algorithm.
 * The algorithm works as follows: tag every word with its most frequent tag, tag unknown words as nouns.
 * As training set for the getPOSOfWord tagger was used nltk.github.com/nltk_data/packages/corpora/treebank.zip.
 */
public class PosTagger {
    public HashMap <String, List<String>> wordAndTags = new HashMap<>();

    /**
     * reads the training set and maps tags to each word
     * @return wordAndTags
     */
    public HashMap<String, List<String>> train() {
        String target_dir = "src\\main\\resources\\treebank/tagged";
        File dir = new File(target_dir);
        File[] files = dir.listFiles();

        for (File f : files) {
            if (f.isFile()) {
                BufferedReader inputStream = null;

                try {
                    inputStream = new BufferedReader(
                            new FileReader(f));
                    String line;

                    while ((line = inputStream.readLine()) != null) {
                        String[] wordInLine = line.split("\\s");
                        for (String word : wordInLine) {
                            if (word.matches(".*/.*")) {
                                String[] splitted = word.split("/");

                                Stemmer ms = new Stemmer();
                                String key = ms.stemWord(splitted[0]);

                                if (wordAndTags.containsKey(key)) {
                                    List<String> temp = wordAndTags.get(key);

                                    temp.add(splitted[1]);
                                    wordAndTags.put(key, temp);
                                }
                                else {
                                    List<String> tagsForWord = new ArrayList<>();
                                    tagsForWord.add(splitted[1]);
                                    wordAndTags.put(key, tagsForWord);
                                }
                            }
                        }


                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        return wordAndTags;

    }

    /**
     * Tags every token with its most frequent tag, unknown words are tagged as nouns.
     * @param tokens
     * @return pos
     */
    public String[] getPOSOfWord(String[] tokens) {
        HashMap<String, List<String>> wordAndTag = train();
        String[] pos = new String[tokens.length];
        Stemmer ms = new Stemmer();
        for (int i = 0; i < tokens.length; i++) {
            if (wordAndTag.containsKey(ms.stemWord(tokens[i]))) {
                List<String> posTag = wordAndTag.get(ms.stemWord(tokens[i]));
                pos[i] = getMostFrequent(posTag);
            } else {
                pos[i] = "NN";
            }
        }

        return pos;
    }

    /**
     * Reference: http://stackoverflow.com/questions/19031213/java-get-most-common-element-in-a-list
     * @param list
     * @return most frequent element in a list
     */
    public String getMostFrequent(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        for (String t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Map.Entry<String, Integer> max = null;
        for (Map.Entry<String, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue())
                max = e;
        }
        return max.getKey();
    }

    /**
     * This method returns words of specific part of speech
     * @param tokens
     * @return words
     */
    public String [] getWordToPOS(String[] tokens, String partOfSpeech) {
        String [] pos = getPOSOfWord(tokens);
        String [] words = new String[tokens.length];

        HashMap<String, String> posTokens = new HashMap<>();
        for (int i = 0; i < pos.length; i++) {
            posTokens.put(tokens[i], pos[i]);
            if (pos[i].equals(partOfSpeech)) {
                words[i] = tokens[i];
            }
        }
        return words;
    }

    /**
     * Writes words of specified part of speech to the file to be used in KUMO project, that we cannot integrate in
     * this project and used the library in a separate project to create word clouds.
     * @param file
     * @param words
     * @throws IOException
     */
    public static void writeWordsOfSpecPOS(String file, String[] words) throws IOException{
        BufferedWriter output = new BufferedWriter(new FileWriter(file));
        for(int i = 0; i < words.length; i++) {
            if(words[i] != null) {
                output.write(words[i] + " ");
            }
        }
        output.flush();
        output.close();
    }
}
