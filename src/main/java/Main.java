import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class Main {
    /**
     * Writes word of specified part of speech to the file. This file is then used to build a word cloud.
     * @param fileName
     * @throws IOException
     */
    public static void assignment4(String fileName) throws IOException {
        //take a file and tokenize it
        Tokenizer tokenizer = new Tokenizer();
        File file = new File(fileName);
        String [] tokens = tokenizer.tokenizeWords(file);
        //create arrays of pos
        PosTagger posTagger = new PosTagger();
        String [] pos = posTagger.getPOSOfWord(tokens);
        //pick nouns
        String [] nouns = posTagger.getWordToPOS(tokens, "NN");
        //write nouns to file
        posTagger.writeWordsOfSpecPOS(fileName+"_pos", nouns);

   }

    public static void main(String[] args) throws IOException {
        assignment4("src\\main\\resources\\reviews\\reviewsFor7883704540.txt");
        assignment4("src\\main\\resources\\reviews\\reviewsForB002LBKDYE.txt");
    }
}
