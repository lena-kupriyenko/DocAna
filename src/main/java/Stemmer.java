/*
References:
http://facweb.cs.depaul.edu/mobasher/classes/csc575/papers/porter-algorithm.html
https://www.eecis.udel.edu/~trnka/CISC889-11S/lectures/dan-porters.pdf
We used these two links as a support for implementing the stemmer.
Our stemmer covers more than 10 rules.
Our stemmer still returns not perfectly stemmed words, but the reason is that the tokenizer wasn't fully correctly implemented.
The tokenizer has to be improved.
 */
public class Stemmer {

    String stemmed = null;
    String lastSuffix = null;

    public String getLastSuffix() {
        return lastSuffix;
    }

    public void setLastSuffix(String lastSuffix) {
        lastSuffix = lastSuffix;
    }
   //in this method we used all six steps implemented further
    public String stemWord(String word) {
        String s1 = step1(word);
        String s2 = step2(s1);
        String s3 = step3(s2);
        String s4 = step4(s3);
        String s5 = step5(s4);
        String s6 = step6(s5);
      return s6;
    }

    public  String[] stemArray(String[] tokens){
        String[] stemmedArray = new String[tokens.length];
            for(int i = 0; i < tokens.length; i++) {
                stemmedArray[i] = stemWord(tokens[i]);
            }
            return stemmedArray;
        }
//this method checks if a word ends with specific suffix
    public boolean end(String word, String suffix) {
        if (word.endsWith(suffix)) {
            lastSuffix = suffix;
            return true;
        } else {
            return false;
        }
    }
//this method replaces the old suffix in a word with an old one
    public String replaceWith(String word, String newSuffix) {
        String newWord = word.replace(getLastSuffix(), newSuffix);
        return newWord;
    }

//this method returns the number of syllables in a word
    public int measure (String word) {
        {
            int count = 0;
            int i = 0;
            while(true)
            {  if (i > word.length()-1) return count;
                if (! isConsonant(word.charAt(i))) break;
                i++;
            }
            i++;
            while(true)
            {  while(true)
            {  if (i > word.length()-1) return count;
                if (isConsonant(word.charAt(i))) break;
                i++;
            }
                i++;
                count++;
                while(true)
                {  if (i > word.length()-1) return count;
                    if (! isConsonant(word.charAt(i))) break;
                    i++;
                }
                i++;
            }
        }
    }
//this method checks if a specific character is a consonant
    public boolean isConsonant(char ch) {
        char newCh = Character.toLowerCase(ch);
        switch (newCh)
        {
            case 'a' :case 'e' :case 'o':case 'i':case 'u':case 'y': return false;
        default: return true;
        }
    }


//this method gets rid of plural forms and -ing

    public String step1(String word) {
        if(measure(word)>0) {
            if (end(word, "sses"))
                stemmed = replaceWith(word, "");
            else if (end(word, "ies"))
                stemmed = replaceWith(word, "i");
            else if (end(word, "ed"))
                stemmed = replaceWith(word, "e");
            else if (end(word, "es"))
                stemmed = replaceWith(word, "e");
            else if (end(word, "ing"))
                stemmed = replaceWith(word, "");
            else if(end(word,"s"))
                stemmed = replaceWith(word,"");
            else
                stemmed = word;
        }
        else {
            stemmed = word;
        }
        return stemmed;
    }
//replace y with i, if the number of syllables is equal or more than 1
    public String step2(String word) {
        if(measure(word)>0) {
            if (end(word, "y"))
                stemmed = replaceWith(word, "i");
        }
        else {
            stemmed = word;
        }

        return stemmed;
    }

 //reducing suffixes
    public String step3(String word) {
        if(measure(word)>0) {
            if(end(word,"ational"))
                stemmed = replaceWith(word,"ate");
            else if(end(word,"tional"))
                stemmed = replaceWith(word, "tion");
            else if(end(word,"enci"))
                stemmed = replaceWith(word,"ence");
            else if(end(word,"anci"))
                stemmed = replaceWith(word,"ance");
            else if(end(word,"izer"))
                stemmed = replaceWith(word,"ize");
            else if(end(word,"abli"))
                stemmed = replaceWith(word,"able");
            else if(end(word,"entli"))
                stemmed = replaceWith(word,"ent");
            else if(end(word,"ousli"))
                stemmed = replaceWith(word, "ous");
            else if(end(word,"ization"))
                stemmed = replaceWith(word,"ize");
            else if(end(word, "ation"))
                stemmed = replaceWith(word, "ate");
            else if(end(word, "ator"))
                stemmed = replaceWith(word, "ate");
            else if(end(word, "alism"))
                stemmed = replaceWith(word, "al");
            else if(end(word, "iveness"))
                stemmed = replaceWith(word, "ive");
            else if(end(word,"aliti"))
                stemmed = replaceWith(word, "al");
            else if(end(word,"iviti"))
                stemmed = replaceWith(word, "ive");
            else if(end(word,"biliti"))
                stemmed = replaceWith(word, "ble");
            else {
                stemmed = word;
            }
        }
        return stemmed;
    }
//replacing specific suffixes with reduced ones
    public String step4(String word) {
        int measure = measure(word);

        if (end(word, "icate") && measure > 0) {
            stemmed = replaceWith(word, "ic");
        } else if (end(word, "ative") && measure > 0) {
            stemmed = replaceWith(word, "");
        } else if (end(word, "alize") && measure > 0) {
            stemmed = replaceWith(word, "al");
        } else if (end(word, "iciti") && measure > 0) {
            stemmed = replaceWith(word, "ic");
        } else if (end(word, "ical") && measure > 0) {
            stemmed = replaceWith(word, "ic");
        } else if (end(word, "ful") && measure > 0) {
            stemmed = replaceWith(word, "");
        } else if (end(word, "ness") && measure > 0) {
            stemmed = replaceWith(word, "");
        } else {
            stemmed = word;
        }

        return stemmed;

    }

    public String step5(String word){
        int measure = measure(word);

        if (end(word, "al") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "ance") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "ence") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "ic") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "able") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "ible") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "ant") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ement") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ment") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ent") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ou") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ism") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ate") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "iti") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ous") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ive") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }
        else if (end(word, "ize") && measure > 1) {
            stemmed = replaceWith(word, "ic");
        }

        else if (end(word, "ion") && measure > 1 ) {
            int index = word.length() - lastSuffix.length();
            if (word.charAt(index - 1) == 's' || word.charAt(index - 1) == 't') {
                stemmed = replaceWith(word, "");
            }
        }
        else{
            stemmed = word;
        }
        //System.out.println(stemmed);
        return stemmed;
    }
    public String step6(String word){
        int measure = measure(word);
        if (end(word, "e") && measure > 1) {
            stemmed = replaceWith(word, "");
        }

        else if (end(word, "e") && measure == 1 && !cvc(word)) {
            stemmed = replaceWith(word, "");
        }

        else if (end(word, "ll") && measure > 1) {
            stemmed = replaceWith(word, "");
        }

        else {
            stemmed = word;
        }

        return stemmed;
    }

    public Boolean cvc(String word){
        int length = word.length();
        isConsonant(word.charAt(length-1));
        if (isConsonant(word.charAt(length-1))){
            if (!isConsonant(word.charAt(length-2))){
                if (isConsonant(word.charAt(length-3))&& word.charAt(length-3)!='w' && word.charAt(length-3)!='y' &&
                        word.charAt(length-3)!='x' ) {
                    return true;
                }
            }
        }
        return false;
    }


}
