import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Ekaterina on 01.11.2015.
 */
public class Reader {
    public List<Product> readFile(String fileName) {
        String line;
        List<Product> products = new ArrayList<>();
        Product product;
        Review review = null;
        String previousAttributeKey = null;
        try {
            File file = new File(fileName);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNext()) {
                line = scanner.nextLine();

                if (line.matches(".*product/productId.*")) {
                    String[] tokens = line.split(":");
                    String prodName = tokens[1].trim().replaceAll("\\\\", ""); //TODO ask, why trim() doesn't work
                    product = containsId(products, prodName); //if product exists

                    if (product == null) { //if a product is new
                        product = new Product(prodName);

                        products.add(product);
                    }
                    review = new Review();
                    product.addReview(review);
                }
                if (line.matches(".*review.*")) { //TODO ask, why "^review" doesn't work
                    line = cleanString(line);
                    String[] tokens = line.split(":", 2);
                    String attribute = tokens[0].trim();
                    String[] attrArr = attribute.split("/");
                    attribute = attrArr[1].trim();
                    previousAttributeKey = attribute;
                    review.setAttribute(attribute, tokens[1].trim().replaceAll("\\\\", ""));
                }
                //if user's input for the profile name, the summary or the next consists of two lines
                if (!line.matches(".*review.*") && !line.matches(".*product/productId.*")) {
                    line = line.trim().replaceAll("\\\\", "");
                    line = cleanString(line);
                    if (!products.isEmpty()) {
                        String valueBefore = null;
                        if (previousAttributeKey.equals("profileName")) {
                           valueBefore = review.getProfileName();
                        }
                        if (previousAttributeKey.equals("summary")) {
                            valueBefore = review.getSummary();
                        }
                        if (previousAttributeKey.equals("text")) {
                            valueBefore = review.getText();
                        }

                        String attributeAfter = valueBefore.concat(" " + line);
                        review.setAttribute(previousAttributeKey, attributeAfter);
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return products;
    }

    public Product containsId(List<Product> list, String id) {
        for (Product object : list) {
            if (object.getProductId().equals(id)) {
                return object;
            }
        }
        return null;
    }

    public List<Product> maxReviews(List<Product> list, int k) {
        List<Product> newList = new ArrayList<>();
        if (k <= list.size()) {
            for (int i = 0; i < k; i++) {
                int maxIndex = i;
                int maxValue = list.get(i).getReviews().size();
                for (int j = i + 1; j < list.size(); j++) {
                    if (list.get(j).getReviews().size() > maxValue) {
                        maxIndex = j;
                        maxValue = list.get(j).getReviews().size();
                    }
                    Product temp = list.get(i);
                    list.set(i, list.get(maxIndex));
                    list.set(maxIndex, temp);
                }
            }
            for (int i = 0; i < k; i ++) {
                newList.add(list.get(i));
            }
            return newList;
        } else {
        return null;
        }
    }

    public String cleanString(String string){
        String newString = null;

        newString = string.replace("<br />"," ").replace("&quot;","'");
        return newString;
    }
}

