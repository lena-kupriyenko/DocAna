/**
 * Created by roman on 11/26/15.
 */

import org.apache.log4j.BasicConfigurator;
import wordcloud.bg.PixelBoundryBackground;
import wordcloud.font.scale.LinearFontScalar;
import wordcloud.nlp.FrequencyAnalyzer;
import wordcloud.*;
import wordcloud.palette.ColorPalette;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainR {
    public static void main(String [] args) throws IOException {

        BasicConfigurator.configure();
        testKumo();

    }

    public static void testKumo() throws IOException {
        final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
        frequencyAnalyzer.setWordFrequencesToReturn(300);
        frequencyAnalyzer.setMinWordLength(4);
        ArrayList<String> stopWords = new ArrayList<String>(Arrays.asList(" "));

        //frequencyAnalyzer.setStopWords(stopWords);

        final List<WordFrequency> wordFrequencies = frequencyAnalyzer.load(getInputStream("text/datarank.txt"));
        final WordCloud wordCloud = new WordCloud(500, 312, CollisionMode.PIXEL_PERFECT);
        wordCloud.setPadding(2);
        wordCloud.setBackground(new PixelBoundryBackground(getInputStream("backgrounds/whale_small.png")));

        wordCloud.setColorPalette(new ColorPalette(new Color(0x4055F1), new Color(0x408DF1), new Color(0x40AAF1), new Color(0x40C5F1), new Color(0x40D3F1), new Color(0xFFFFFF)));
        wordCloud.setFontScalar(new LinearFontScalar(10, 40));
        wordCloud.build(wordFrequencies);
        wordCloud.writeToFile("whale_wordcloud_small.png");
    }

    private static InputStream getInputStream(String filePath){
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath);
    }
}
