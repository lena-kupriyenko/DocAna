
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * This class contains two methods: the first one splits the data from the cleaned file into single tokens.
 * The second one splits the data from the cleaned file into sentences.
 */
public class Tokenizer {

    public String [] tokens;

    public  String [] tokenizeWords (File file) throws FileNotFoundException {
        String text = new Scanner(file).useDelimiter("\\A").next();
        //read the cleaned file and store it in a single string
        //String text = string.useDelimiter("\\A").next();
        String splitRegex ="\\s+|" + //split by whitespaces
                "((?<![0-9])(?=\\.))|" + //don't split numbers with "."
                "((?<![0-9])(?=,))|" + //don't split numbers with ","
                "((?<=[:;!'`])|(?=[:;!'`]))|" + // split by ":", "!", ";", "'"
                "((?<=[(])|(?=[)]))";//split by parantheses

        String[] tokens = text.split(splitRegex);

        return tokens;
    }

    public String [] tokenizeSen(File file) throws FileNotFoundException {
        //read the cleaned file and store it in a single string
        String text = new Scanner(file).useDelimiter("\\A").next();
        String[] splitted = text.split("(?<=[a-z])\\.!?\\s+");//split text into sentences by ".", "!", "?".

        return splitted;
    }
}
