/**
 * The class stores information about the Review.
 */
public class Review {

    private String helpfulness;
    private int totalReviews;
    private int positiveReviews;
    private double score;
    private Long time;
    private String summary;
    private String text;
    private String user;
    private String profileName;

    public String getHelpfulness() {
        return helpfulness;
    }

    public void setHelpfulness(String helpfulness) {
        this.helpfulness = helpfulness;
    }
    public int getTotalReviews() {
        return totalReviews;
    }
    public void setTotalReviews(int totalReviews) {

        this.totalReviews = totalReviews;
    }

    public int getPositiveReviews() {
        return positiveReviews;

    }

    public void setPositiveReviews(int positiveReviews) {
        this.positiveReviews = positiveReviews;

    }

    public Long getTime() {
        return time;

    }

    public void setTime(Long time) {
        this.time = time;

    }

    public double getScore() {
        return score;

    }

    public void setScore(double score) {
        this.score = score;

    }

    public String getSummary() {
        return summary;

    }

    public void setSummary(String summary) {
        this.summary = summary;

    }

    public String getText() {
        return text;

    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user; }

    public void setUser(String user) {
        this.user = user; }

    public String getProfileName() {
        return profileName; }

    public void setProfileName(String profileName) {
        this.profileName = profileName; }

    public void setAttribute(String attributeName, String value) {
        if (attributeName.equals("userId")) {
            this.setUser(value);
        }
        if (attributeName.equals("profileName")) {
            this.setProfileName(value);
        }
        if (attributeName.equals("helpfulness")) {
            this.setHelpfulness(value);
        }
        if (attributeName.equals("score")) {
            this.setScore(Double.parseDouble(value));
        }
        if (attributeName.equals("time")) {
            this.setTime(Long.valueOf(value));
        }

        if (attributeName.equals("summary")) {
            this.setSummary(value);
        }
        if (attributeName.equals("text")) {
            this.setText(value);
        }

    }
}