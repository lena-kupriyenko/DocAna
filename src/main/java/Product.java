import java.util.ArrayList;
import java.util.List;

/**
 * The class stores information about the Product.
 */
public class Product {

    private String productId;
    private List<Review> reviews;

    public  Product(String productId){
        this.productId = productId;
        this.reviews = new ArrayList<>();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void addReview (Review review) {
        this.reviews.add(review);
    }
}